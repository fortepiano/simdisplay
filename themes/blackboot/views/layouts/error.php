<!DOCTYPE html>
<html>
<head>
    <title> BPMPPT - Manajemen Display</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
  <!--  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Abel|Open+Sans:400,600" rel="stylesheet" />-->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/icon/favicon.ico">

    <style>

    

        html,body {
            height:100%;
        }
        .container-error{
            
            padding: 10px;
            text-align: center;
        }
        a:hover, a:focus {
        color: #ff3333;
        text-decoration: underline;
        }

        a:active, a:hover {
        outline: 0;
        }
        
        a {
        color: #ff3333;
        text-decoration: none;
        }

        body {
            background: url('<?php echo Yii::app()->theme->baseUrl.'/icon/back.jpg' ?>') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            color:#black;
            background-color:#333;
            font-family: 'Open Sans',Arial,Helvetica,Sans-Serif;
            padding-top: 100px;
        }
            span.required{
                color:white;
                opacity: 0.1;
            }
       
            .form-signin
            {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }            
            .form-signin .form-signin-heading, .form-signin .checkbox
            {
                margin-bottom: 10px;
            }
            .form-signin .checkbox
            {
                font-weight: normal;
            }
            .form-signin .form-control
            {
                position: relative;
                font-size: 16px;
                height: auto;
                padding: 10px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-signin .form-control:focus
            {
                z-index: 2;
            }
            .form-signin input[type="text"]
            {
                margin-bottom: -1px;
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0;
            }
            .form-signin input[type="password"]
            {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
            .account-wall
            {
                margin-top: 20px;
                opacity: 0.8;
                padding: 40px 0px 20px 0px;
                background-color: #f7f7f7;
                -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            }
            .login-title
            {
                color: #555;
                font-size: 18px;
                font-weight: 400;
                display: block;
            }
            .profile-img
            {
                width: 96px;
                height: 96px;
                margin: 0 auto 10px;
                display: block;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
                border-radius: 50%;
            }
            .need-help
            {
                margin-top: 10px;
            }
            .new-account
            {
                display: block;
                margin-top: 10px;
            }
            .btn-primary.btn-block.btn.btn-default{
                background-color:#5A427F;
                color:#FFF;
            }

        </style>
    </head>
 
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                  <h3 class="text-center"> <?php echo CHtml::Image(Yii::app()->theme->baseUrl."/icon/logokab.png",'',array('width'=>'100'))?>
                  <h4 class="text-center"><strong>BPMPPT - Manajemen Display</strong></h4>
                    <p class="lead text-center">Kabupaten Bandung Barat</p>
                    <?php echo $content ?>
            </div>
           
        </div>
    </div>
</div>
</body>
</html>