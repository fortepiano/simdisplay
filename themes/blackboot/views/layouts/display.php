<?php
Yii::app()->clientscript
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/flexslider.css')
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/animate.css')
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/display.css')
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.flexslider.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.marquee.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.lettering-0.6.1.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.textillate.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clock.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/switch-tab.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slider.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/marquee.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/text.js', CClientScript::POS_END)

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>BPMPPT- Display</title>
    <meta name="language" content="en"/>
    <meta http-equiv="refresh" content="60000"/>
</head>
<body>


<div class="container-full">

    <div class="head">

        <div class="row">
            <div class="col-lg-6">
                <div class="icon-atas">
                    <img src=<?php echo Yii::app()->theme->baseUrl . '/icon/favicon.ico' ?>>
                    <?php echo Config::model()->find()->text_atas; ?>

                </div>
            </div>
            <div class="col-lg-2">
                <!-- Empty -->
            </div>
            <div class="col-lg-4">
                <div class="clock pull-right">
                    <ul>
                        <li id="Date"></li>
                        <br>
                        <li id="hours"></li>
                        <li id="point">:</li>
                        <li id="min"></li>
                        <li id="point">:</li>
                        <li id="sec"></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="atas-marquee">
                    <?php echo Config::model()->find()->pesan_atas; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-7">


                <?php
                $this->widget('ext.jwplayer.Jwplayer', array(
                    'width' => '100%',
                    'height' => 500,
                    'autostart' => false,
                    'file' => Video::model()->getUrlVideo(Config::model()->find()->video_aktif), // the file of the player, if null we use demo file of jwplayer
                    'image' => '', // the thumbnail image of the player, if null we use demo image of jwplayer
                    'options' => array(
                        'controlbar' => 'none'
                    )
                ));
                ?>
            </div>

            <div class="col-lg-5">
                <div class="syarat-marquee">
                    <?php
                    $model = Jenisperizinan::model()->findAll();

                    foreach ($model as $data) {
                        echo "<a>$data->nama_jenis ($data->singkatan_jenis)</a> <br>";
                        echo "Waktu Proses : $data->waktu_proses Hari<br>";
                    }
                    ?>
                </div>

            </div>
            <hr>
            <div class="row">
                <div class="col-lg-7">

                    <div id="hiddentab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#chart" role="tab" data-toggle="tab">Chart</a></li>
                            <li><a href="#table" role="tab" data-toggle="tab">Table</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="chart">
                                <?php
                                $this->widget(
                                    'booster.widgets.TbHighCharts',
                                    array(

                                        'options' => array(
                                            'theme' => 'dark-unica',
                                            'chart' => [
                                                'type' => 'column'
                                            ],
                                            'title' => array(
                                                'text' => 'Grafik Realisasi Tahunan',
                                                'x' => -20 //center
                                            ),
                                            'xAxis' => array(
                                                'categories' => Realisasi::model()->getTahun(),
                                            ),
                                            'yAxis' => array(
                                                'title' => array(
                                                    'text' => 'Realisasi',
                                                ),
                                                'plotLines' => array(
                                                    array(
                                                        'value' => 0,
                                                        'width' => 1,
                                                        'color' => '#808080'
                                                    )
                                                ),

                                            ),
                                            'plotOptions' => array(
                                                'column' => array(
                                                    'dataLabels' => array(
                                                        'enabled' => false,
                                                    )
                                                ),
                                            ),
                                            'tooltip' => array(
                                                'valuePrefix' => 'Rp.',
                                                'valueSuffix' => '.00'
                                            ),
                                            'legend' => array(
                                                'layout' => 'vertical',
                                                'align' => 'right',
                                                'verticalAlign' => 'middle',
                                                'borderWidth' => 0
                                            ),
                                            'series' => array(
                                                array(

                                                    'name' => 'PMA',
                                                    'data' => Realisasi::model()->getPma(),
                                                ),
                                                array(

                                                    'name' => 'PMDN',
                                                    'data' => Realisasi::model()->getPmdn(),
                                                ),
                                            )
                                        ),
                                        'htmlOptions' => array(
                                            'style' => 'min-width: 100%; height: 320px; margin: 0 auto'
                                        )
                                    )
                                );
                                ?>
                            </div>
                            <div class="tab-pane fade" id="table">
                                <?php
                                $this->widget('booster.widgets.TbGridView', array(
                                    'id' => 'Perizinan-grid',
                                    'dataProvider' => Realisasi::model()->getProvider(),
                                    'cssFile' => false,
                                    'summaryText' => '',
                                    'columns' => array(
                                        array(
                                            'header' => 'No',
                                            'value' => '$row+1',
                                        ),
                                        array(
                                            'header' => '<a>Tahun</a>',
                                            'value' => '$data->tahun',
                                        ),
                                        array(
                                            'header' => '<a>PMA</a>',
                                            'value' =>
                                                function ($data) {
                                                    $return = Yii::app()->formatter->formatNumber($data->getPmaByTahun($data->tahun));
                                                    return $return;
                                                },
                                        ),
                                        array(
                                            'header' => '<a>PMDN</a>',
                                            'value' =>
                                                function ($data) {
                                                    $return = Yii::app()->formatter->formatNumber($data->getPmdnByTahun($data->tahun));
                                                    return $return;
                                                },
                                        ),
                                        array(
                                            'header' => '<a>Total</a>',
                                            'value' =>
                                                function ($data) {
                                                    $return = Yii::app()->formatter->formatNumber($data->getJumlahByTahun($data->tahun));
                                                    return $return;
                                                },
                                        ),
                                    ),
                                )); ?>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php
                            foreach (Galeri::model()->findAll() as $data):
                                echo "<li>";
                                echo "<div class='slide-desc'> $data->keterangan </div>";
                                echo "<img src=" . Galeri::model()->getUrlImage($data->id_galeri) . "/>";
                                echo "</li>";
                            endforeach;

                            ?>

                        </ul>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="hiddentab-bottom">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#selesai" role="tab" data-toggle="tab">Selesai</a></li>
                            <li><a href="#proses" role="tab" data-toggle="tab">Proses</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="selesai">
                                <?php
                                $this->widget('booster.widgets.TbGridView', array(
                                    'id' => 'Perizinans-grid',
                                    'dataProvider' => Perizinan::model()->getProvider(1),
                                    'cssFile' => false,
                                    'summaryText' => '',
                                    'columns' => array(
                                        array('header' => 'No',
                                            'value' => '$row+1',
                                        ),
                                        array(
                                            'header' => 'Jenis Perizinan',
                                            'name' => 'id_jenisperizinan',
                                            'value' =>
                                                function ($data) {
                                                    if ($data->id_jenisperizinan != ''):
                                                        $return = $data->idJenisperizinan->singkatan_jenis;
                                                    else:
                                                        $return = '';
                                                    endif;

                                                    return $return;
                                                },
                                            'type' => 'html',

                                        ),
                                        'nama_pemohon',
                                        'alamat',
                                        'perusahaan',
                                        array(
                                            'header' => 'Status',
                                            'name' => 'status',
                                            'value' =>
                                                function ($data) {
                                                    if ($data->status == '1'):
                                                        $return = '<span class="blue">Selesai</span>';
                                                    else:
                                                        $return = '<span class="red">Proses</span>';
                                                    endif;
                                                    return $return;
                                                },
                                            'type' => 'html',
                                        ),

                                    ),
                                )); ?>

                            </div>
                            <div class="tab-pane fade" id="proses">
                                <?php
                                $this->widget('booster.widgets.TbGridView', array(
                                    'id' => 'Perizinans-grid',
                                    'dataProvider' => Perizinan::model()->getProvider(0),
                                    'cssFile' => false,
                                    'summaryText' => '',
                                    'columns' => array(
                                        array('header' => 'No',
                                            'value' => '$row+1',
                                        ),
                                        array(
                                            'header' => 'Jenis Perizinan',
                                            'name' => 'id_jenisperizinan',
                                            'value' =>
                                                function ($data) {
                                                    if ($data->id_jenisperizinan != ''):
                                                        $return = $data->idJenisperizinan->singkatan_jenis;
                                                    else:
                                                        $return = '';
                                                    endif;

                                                    return $return;
                                                },
                                            'type' => 'html',

                                        ),
                                        'nama_pemohon',
                                        'alamat',
                                        'perusahaan',
                                        array(
                                            'header' => 'Status',
                                            'name' => 'status',
                                            'value' =>
                                                function ($data) {
                                                    if ($data->status == '1'):
                                                        $return = '<span class="blue">Selesai</span>';
                                                    else:
                                                        $return = '<span class="red">Proses</span>';
                                                    endif;
                                                    return $return;
                                                },
                                            'type' => 'html',
                                        ),

                                    ),
                                )); ?>

                            </div>
                        </div>

                    </div>

                </div>


            </div>
        </div>
    </div>


</body>
</html>

<div id="footer">
    <p class="vertic">
        <?php echo Config::model()->find()->pesan_bawah; ?>
    </p>
</div>