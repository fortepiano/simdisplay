<?php $this->beginContent('//layouts/main'); ?>   
 <div class="title-box">
 <?php if($this->title != ''):
        echo "<div class='title'>$this->title </div>";
    endif;
    ?>
</div>
<div class="main">

	 <?php if(isset($this->breadcrumbs)):?>
				<?php
                    $this->widget(
                        'booster.widgets.TbBreadcrumbs',
                        array(
                            'links' => $this->breadcrumbs,
                        )
                    );
                    ?>
	<?php endif?>
   
    <?php if(isset($this->menu)):
        $active = '';
        echo '<ul class="nav nav-tabs">';
        foreach ($this->menu as $key => $value):
            if(strtolower($key) == 'active'):
                $active = $value;
            else:            
                if($key == $active):
                    echo '<li class ="active"><a href="'.Yii::app()->createUrl($value).'">'.$key.'</a></li>';
                    echo '<link rel="prerender" href="'.Yii::app()->createUrl($value).'"/>';
                else:
                    echo '<li><a href="'.Yii::app()->createUrl($value).'">'.$key.'</a></li>';
                    echo '<link rel="prerender" href="'.Yii::app()->createUrl($value).'"/>';
                endif;   
            endif;
        endforeach;
        echo '</ul>';
    endif;
    ?>


			<?php echo $content; ?>
		</div><!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>
