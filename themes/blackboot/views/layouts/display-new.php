<?php
Yii::app()->clientscript
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/flexslider.css')
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/animate.css')
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/video-js.min.css')
    ->registerCssFile(Yii::app()->theme->baseUrl . '/css/display.css')
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.flexslider.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.marquee.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.lettering-0.6.1.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.textillate.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/clock.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/switch-tab.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slider.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/marquee.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/text.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/video.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/youtube.min.js', CClientScript::POS_END)
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>BPMPPT- Display</title>
    <meta name="language" content="en"/>
    <meta http-equiv="refresh" content="60000"/>
    <!-- If you'd like to support IE8 -->
    <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
</head>
<body>


<div class="container-full">

    <div class="head">

        <div class="row">
            <div class="col-lg-4">
                <div class="icon-atas">
                    <img src=<?php echo Yii::app()->theme->baseUrl . '/icon/logokab.png' ?>>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="text-atas">
                    Selamat <span>Datang</span>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon-atas pull-right">
                    <img src=<?php echo Yii::app()->theme->baseUrl . '/images/logo.jpeg' ?> height="98">
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="body-content">
        <div class="container">
            <div class="row ml-01em">
                <div class="col-lg-1 kbb mb-1em">
                    KBB
                </div>
                <div class="col-lg-11">
                    <div class="atas-marquee">
                        <?php echo Config::model()->find()->pesan_atas; ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-6">
                    <?php if (Helper::IsYoutubeUrl(Video::model()->getUrlVideo(Config::model()->find()->video_aktif))): ?>
                        <video
                                id="my-video"
                                class="video-js vjs-default-skin"
                                preload="auto"
                                autoplay
                                loop
                                data-setup='{
                                "fluid":true,
                                "techOrder": ["youtube"],
                                "sources": [
                                        {
                                            "type": "video/youtube",
                                            "src": "<?=Video::model()->getUrlVideo(Config::model()->find()->video_aktif)?>",
                                            "youtube": { "iv_load_policy": 1 }
                                        }
                                ]
                            }'
                        >
                        </video>
                    <?php else: ?>
                        <video id="my-video" class="video-js" preload="auto" autoplay loop data-setup='{"fluid": true}'>
                            <source src=<?php echo Video::model()->getUrlVideo(Config::model()->find()->video_aktif) ?> type='video/mp4'>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser
                                that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                                    video</a>
                            </p>
                        </video>
                    <?php endif ?>
                </div>
                <div class="col-lg-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php
                            foreach (Galeri::model()->findAll() as $data):
                                echo "<li>";
                                echo "<div class='slide-desc'> $data->keterangan </div>";
                                echo "<img src=" . Galeri::model()->getUrlImage($data->id_galeri) . " heigth/>";
                                echo "</li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-content">
        <br>
        <div class="row ml-01em">
            <div class="col-lg-2">
                <div class="clock">
                    <ul>
                        <li id="Date"></li>
                        <br>
                        <li id="hours"></li>
                        <li id="point">:</li>
                        <li id="min"></li>
                        <li id="point">:</li>
                        <li id="sec"></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="syarat-marquee">
                    <?php echo Config::model()->find()->pesan_bawah; ?>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>
