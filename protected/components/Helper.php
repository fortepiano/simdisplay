<?php
class Helper {

	public static  function sesi($index)
	{		
		if(isset(Yii::app()->session[$index])):
			$hasil = Yii::app()->session[$index];
		else:
			$hasil = false;
		endif;

		return $hasil;
	}

	public static function cleanData($data) 
    {

     $data = str_replace(',', '', $data);

     return $data;
 	}

    public static function IsYoutubeUrl($url)
    {
        return preg_match("#^https?://(?:www\.)?youtube.com#", $url);
    }
}