<?php

/**
 * This is the model class for table "realisasi".
 *
 * The followings are the available columns in table 'realisasi':
 * @property string $id_realisasi
 * @property integer $kategori
 * @property integer $tahun
 * @property string $jumlah
 */
class Realisasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'realisasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kategori, tahun', 'numerical', 'integerOnly'=>true),
			array('jumlah', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_realisasi, kategori, tahun, jumlah', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_realisasi' => 'Id Realisasi',
			'kategori' => 'Kategori',
			'tahun' => 'Tahun',
			'jumlah' => 'Jumlah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_realisasi',$this->id_realisasi,true);
		$criteria->compare('kategori',$this->kategori);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('jumlah',$this->jumlah,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Realisasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	//Chart

	public function getTahun()
	{
		$data = Yii::app()->db->createCommand('SELECT distinct tahun FROM realisasi ORDER BY tahun ASC')->queryAll();
		$hasil = array();

		foreach ($data as $key => $value):
			array_push($hasil, $value['tahun']);	
		endforeach;

		return $hasil;
	}

	public function getPma()
	{
		$perintah ="SELECT
					Sum(realisasi.jumlah) AS jumlah
					FROM
					realisasi
					WHERE
						realisasi.kategori = 0
					GROUP BY
						realisasi.tahun
					ORDER BY
						realisasi.tahun ASC
					";
		$data = Yii::app()->db->createCommand($perintah)->queryAll();
		$hasil = array();

		foreach ($data as $key => $value):
			array_push($hasil, floatval($value['jumlah']));	
		endforeach;

		return $hasil;
	}

	public function getPmdn()
	{
		$perintah ="SELECT
					Sum(realisasi.jumlah) AS jumlah
					FROM
					realisasi
					WHERE
						realisasi.kategori = 1
					GROUP BY
						realisasi.tahun
					ORDER BY
						realisasi.tahun ASC
					";
		$data = Yii::app()->db->createCommand($perintah)->queryAll();
		$hasil = array();

		foreach ($data as $key => $value):
			array_push($hasil, floatval($value['jumlah']));	
		endforeach;

		return $hasil;
	}


	//Tabel
	public function getProvider()
	{
		$dataProvider=new CActiveDataProvider('Realisasi', array(
		    'criteria'=>array(
		        'order'=>'tahun ASC',
		        'distinct'=>true,
		        'select'=>'tahun'
		    ),		   
		));

		return $dataProvider;
	}

	public function getPmaBytahun($tahun)
	{
		$perintah = "SELECT realisasi.jumlah AS jumlah from realisasi where kategori = 0 AND tahun = $tahun";
		$hasil = Yii::app()->db->createCommand($perintah)->queryScalar();
		return floatval($hasil);
	}

	public function getPmdnBytahun($tahun)
	{
		$perintah = "SELECT realisasi.jumlah AS jumlah from realisasi where kategori = 1 AND tahun = $tahun";
		$hasil = Yii::app()->db->createCommand($perintah)->queryScalar();
		return floatval($hasil);
	}

	public function getJumlahBytahun($tahun)
	{
		$pma = $this->getPmaBytahun($tahun);
		$pmdn = $this->getPmdnBytahun($tahun);

		$jumlah = floatval($pma) + floatval($pmdn);
		return $jumlah;

	}
}
