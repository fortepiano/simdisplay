<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property string $id_config
 * @property string $video_aktif
 * @property string $pesan_atas
 * @property string $pesan_bawah
 *
 * The followings are the available model relations:
 * @property Video $videoAktif
 */
class Config extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('video_aktif', 'required'),
			array('video_aktif', 'length', 'max'=>11),
			array('pesan_atas, pesan_bawah,text_atas', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_config, video_aktif, pesan_atas, pesan_bawah,text_atas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'videoAktif' => array(self::BELONGS_TO, 'Video', 'video_aktif'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_config' => 'Id Config',
			'video_aktif' => 'Video Aktif',
			'pesan_atas' => 'Running Text Atas',
			'pesan_bawah' => 'Running Text Bawah',
			'text_atas'=> 'Text Atas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_config',$this->id_config,true);
		$criteria->compare('video_aktif',$this->video_aktif,true);
		$criteria->compare('pesan_atas',$this->pesan_atas,true);
		$criteria->compare('pesan_bawah',$this->pesan_bawah,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
