<?php

class PerizinanController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('?')
					),
				
				);
	}

	public function actionIndex()
	{
		$model = new Perizinan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Perizinan']))
			$model->attributes=$_GET['Perizinan'];

		$this->render('index',array('model'=>$model));
	}

	public function actionTambah()
	{

		$model = new Perizinan;
		if(isset($_POST['Perizinan'])):
			$model->attributes = $_POST['Perizinan'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Perizinan',
			'Data Perizinan'=>'Perizinan/index',
			'Tambah Perizinan'=>'Perizinan/tambah',
		);

		$this->render('form',array('model'=>$model));
	}
	public function actionUbah($id)
	{

		$model = $this->loadModel($id);
		if(isset($_POST['Perizinan'])):
			$model->attributes = $_POST['Perizinan'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Perizinan',
			'Data Perizinan'=>'Perizinan/index',
			'Tambah Perizinan'=>'Perizinan/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionHapus($id)
	{
        $model=$this->loadModel($id);       
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function loadModel($id)
	{

		$model=Perizinan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exists.');
		return $model;
	}


}