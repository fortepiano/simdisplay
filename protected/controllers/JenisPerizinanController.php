<?php

class JenisPerizinanController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','Tambah','Ubah','Hapus'),
						'users'=>array('?')
					),
				
				);
	}
	public function actionIndex()
	{
		$model = new Jenisperizinan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jenisperizinan']))
			$model->attributes=$_GET['Jenisperizinan'];

		$this->render('index',array('model'=>$model));
	}

	public function actionTambah()
	{

		$model = new Jenisperizinan;
		if(isset($_POST['Jenisperizinan'])):
			$model->attributes = $_POST['Jenisperizinan'];
			$model->dihapus = 0;
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Jenis Perizinan',
			'Data Jenis Perizinan'=>'JenisPerizinan/index',
			'Tambah Jenis Perizinan'=>'JenisPerizinan/tambah',
		);

		$this->render('form',array('model'=>$model));
	}
	public function actionUbah($id)
	{

		$model = $this->loadModel($id);
		if(isset($_POST['Jenisperizinan'])):
			$model->attributes = $_POST['Jenisperizinan'];
			$model->dihapus = 0;
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Jenis Perizinan',
			'Data Jenis Perizinan'=>'Jenisperizinan/index',
			'Tambah Jenis Perizinan'=>'Jenisperizinan/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionHapus($id)
	{

		$model = $this->loadModel($id);
		$model->dihapus = 1;

		if($model->save()):
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		endif;
	}

	public function loadModel($id)
	{

		$model=Jenisperizinan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exists.');
		return $model;
	}


}