<?php

class UserController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}

	public function accessRules()
	{
		// return external action classes, e.g.:
		return array(
			array('allow',
					'actions'=>array('Index','Add','Edit','Delete'),
					'users'=>array('@'),					
				),
				array('deny',
					'actions'=>array('Index','Add','Edit','Delete'),
					'users'=>array('?')
				),
			
			);
	}

	public function actionIndex()
	{
		$model = new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];
		$this->render('index',array('model'=>$model));
	}

	
	public function actionLogin()
	{
		$this->layout="//layouts/login";
		if(Yii::app()->user->isGuest):
			$model=new LoginForm;		
			if(isset($_POST['LoginForm'])):
				$model->attributes=$_POST['LoginForm'];
				if($model->validate() && $model->login()):						
					$this->redirect(array("Video/Index"));					
				endif;
			 endif;
        $this->render('form-login',array('model'=>$model));
		else:
			$this->redirect(array("Video/Index"));;
		endif;
    
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array("Site/Index"));
	}

	public function actionAdd()
	{
		$model = new User;
		if(isset($_POST['User'])):
			$model->username = $_POST['User']['username'];
				$salt = openssl_random_pseudo_bytes(22);
				$salt = '$2a$%13$' . strtr($salt, array('_' => '.', '~' => '/'));
				$password_hash = crypt($_POST['User']['password'], $salt);
				$password_hash2 = crypt($_POST['User']['password_repeat'], $salt);
				$model->password = $password_hash;
				$model->password_repeat = $password_hash2;
			if($model->save()):
				$this->redirect(array('Index'));
			endif;
		endif;

		$this->menu =array(
			'Active' => 'Tambah User',
			'Data User'=>'User/index',
			'Tambah User'=>'User/add',
			);

		$this->render('form',array('model'=>$model));
	}

	public function actionEdit($id)
	{
		$model = $this->loadModel($id);
		$model->password_repeat = $model->password;
		if(isset($_POST['User'])):
			$model->username = $_POST['User']['username'];
				$salt = openssl_random_pseudo_bytes(22);
				$salt = '$2a$%13$' . strtr($salt, array('_' => '.', '~' => '/'));
				if ($model->password === crypt($_POST['User']['password'], $salt)):					
					$model->password_repeat = $model->password;
				else:					
					$password_hash = crypt($_POST['User']['password'], $salt);
					$password_hash2 = crypt($_POST['User']['password_repeat'], $salt);
					$model->password = $password_hash;
					$model->password_repeat = $password_hash2;					
				endif;
			if($model->save()):
				$this->redirect(array('Index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Ubah User',
			'Data User'=>'User/index',
			'Ubah User'=>"User/edit/$id",
		);

		$this->render('form',array('model'=>$model));
	}

	public function loadModel($id)
	{

		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not existss.');
		return $model;
	}
	public function actionDelete($id)
	{
        $model=$this->loadModel($id);       
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	
}