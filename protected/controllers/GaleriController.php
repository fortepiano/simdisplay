<?php

class GaleriController extends Controller
{
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',			
		);
	}
	public function accessRules(){
		return array(
				array('allow',
						'actions'=>array('Index','Tambah','Ubah','Hapus','Detail'),
						'users'=>array('@'),					
					),
					array('deny',
						'actions'=>array('Index','Tambah','Ubah','Hapus','Detail'),
						'users'=>array('?')
					),
				
				);
	}

	public function actionIndex()
	{
		$model = new Galeri('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Galeri']))
			$model->attributes=$_GET['Galeri'];

		$this->render('index',array('model'=>$model));
	}

	public function actionTambah()
	{

		$model = new Galeri;
		if(isset($_POST['Galeri'])):
			$model->attributes = $_POST['Galeri'];
			$model->status = 1;
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Galeri',
			'Data Galeri'=>'Galeri/index',
			'Tambah Galeri'=>'Galeri/tambah',
		);

		$this->render('form',array('model'=>$model));
	}
	public function actionUbah($id)
	{

		$model = $this->loadModel($id);
		if(isset($_POST['Galeri'])):
			$model->attributes = $_POST['Galeri'];
			if($model->save()):
				$this->redirect(array('index'));
			endif;
		endif;


		$this->menu =array(
			'Active' => 'Tambah Galeri',
			'Data Galeri'=>'Galeri/index',
			'Tambah Galeri'=>'Galeri/tambah',
		);

		$this->render('form',array('model'=>$model));
	}

	public function actionHapus($id)
	{
        $model=$this->loadModel($id);       
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionDetail($id)
	{
		$model = $this->loadModel($id);

		$this->render('detail',array('model'=>$model));
	}
	
	
	public function loadModel($id)
	{

		$model=Galeri::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exists.');
		return $model;
	}


}