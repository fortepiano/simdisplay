<?php
$this->title = "List User";
$this->breadcrumbs=array(
	'Data User'=>array('index'),
);
$this->menu =array(
	'Active' => 'Data User',
	'Data User'=>'User/index',
	'Tambah User'=>'User/add',
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'User-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>false,
	'summaryText' => '',
	'columns'=>array(		
		array(
			'header'=>'Username',
			'name'=>'username',
			'value'=>'$data->username',		
		),		
		array(
			    'class'=>'CButtonColumn',
			    'template'=>'{update}{delete}',
			    'buttons'=>
			    array(
			        'update' => array(
			            'label'=>'Edit',			         
			            'url'=>'Yii::app()->createUrl("User/Edit", array("id"=>$data->id_user))',
			        ),
			         'delete' => array(
			            'label'=>'Delete',			         
			            'url'=>'Yii::app()->createUrl("User/Delete", array("id"=>$data->id_user))',
			        ),					       
			    ),
			),
	),

	
)); 

