<?php
$this->title= "Formulir Video";
$this->breadcrumbs=array(
    'Data Video'=>array('Video/index'),
);


?>

<!-- Formulir -->
<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'FormVideo',
        'htmlOptions' => array('class' => 'well','enctype'=>'multipart/form-data'), // for inset effect
    )
);
echo $form->textFieldGroup($model, 'judul_video');

echo "<p class='title'>Upload Video / Sertakan Url</p>";
echo $form->textFieldGroup($model,'url_video');

echo "<hr>";
echo $form->fileFieldGroup($model,'file_video');

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Simpan')
);
 
$this->endWidget();
unset($form);
