<?php
$this->title = "List Video";
$this->breadcrumbs = array(
    'Video' => array('index'),
);
?>
    <br>
    <p class="title"><?php echo $model->judul_video ?></p>
    <br>
<?php if (Helper::IsYoutubeUrl(Video::model()->getUrlVideo($model->id_video))): ?>
    <video
            id="my-video"
            class="video-js vjs-default-skin"
            preload="auto"
            autoplay
            loop
            data-setup='{
                                "fluid":true,
                                "techOrder": ["youtube"],
                                "sources": [
                                        {
                                            "type": "video/youtube",
                                            "src": "<?= Video::model()->getUrlVideo($model->id_video) ?>",
                                            "youtube": { "iv_load_policy": 1 }
                                        }
                                ]
                            }'
    >
    </video>
<?php else: ?>
    <video id="my-video" class="video-js" preload="auto" autoplay loop data-setup='{"fluid": true}'>
        <source src=<?php echo Video::model()->getUrlVideo($model->id_video) ?> type='video/mp4'>
        <p class="vjs-no-js">
            To view this video please enable JavaScript, and consider upgrading to a web browser
            that
            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                video</a>
        </p>
    </video>
<?php endif ?>